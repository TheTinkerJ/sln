class Rational (n:Int , d:Int){
  
  //参数列表合法检验
  require(d != 0)  //分母不为0
  
  //参数构造阶段
  private val g = gcd( n.abs , d.abs ) //求解最大公约数 在初始化阶段即完成最简
  val numer = n/g     //解决自身方法无法访问参数列表问题
  val denom = d/g     
  def this(n:Int) = this(n,1)       //解决多参数构造器问题
  
  //方法定义 + - * / 使用重载来满足 Int 和 Rational的多种参数输入

  def +(that:Rational) : Rational = 
    new Rational(this.numer*that.denom + that.numer*this.denom,this.denom*that.denom)
  
  def +(i:Int):Rational = 
    new Rational(this.numer+i*this.denom,this.denom)

  def -(that:Rational) : Rational =
    new Rational(this.numer*that.denom - that.numer*this.denom,this.denom*that.denom)

  def -(i:Int):Rational =
    new Rational(this.numer-i*this.denom,this.denom)

  def *(that:Rational) : Rational =
    new Rational(this.numer*that.numer,this.denom*that.denom)
  
  def *(i:Int):Rational =
    new Rational(this.numer*i,this.denom)

  def /(that:Rational) : Rational =
    new Rational(this.numer*that.denom ,this.denom*this.numer)

  def /(i:Int):Rational =
    new Rational(this.numer,this.denom*i)

  
  override def toString = numer +"/"+ denom  //覆盖超类java.lang.object的方法
  private def gcd(a:Int,b:Int):Int = 
    if( b == 0 ) a else gcd ( b , a % b )//解决分子分母化简
}
