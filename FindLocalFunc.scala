import scala.io.Source

object LongLines {
    def processFiles(filename:String,width:Int){

        //将方法定义到函数中：本地方法-函数的函数
        def processLine(filename:String,width:Int,line:String){
            if(line.length > width)
                println(filename+" : "+line.trim)
        }

        val source = Source.fromFile(filename)
        for(line <- source.getLines)
            processLine(filename,width,line)
    }
}

object FindLongLines {

    def main(args:Array[String]){
        val width = args(0).toInt
        for( arg <- args.drop(1)){
            LongLines.processFiles(arg,width)
        }
    }
}