# Scala_day2

- 使用类型参数化数组Array[Type]

如何解释`类型`与`参数化 `?

1. 类似java使用new实例化对象
2. 实例化过程中可以用值和类型使对象参数化

```scala
thetinkerj@JJ:~/Scala/SLN/sln$ scala
Welcome to Scala 2.12.7 (OpenJDK 64-Bit Server VM, Java 10.0.2).
Type in expressions for evaluation. Or try :help.
//使用scala的类型推断机制的定义方式
scala> val greetings = new Array[String](3)
greetings: Array[String] = Array(null, null, null)
scala> greetings(0)="one"
scala> greetings(1)="two"
scala> greetings(2)="three"
scala> for (i <- 0 to 2)
     | {
     |   print(greetings(i))
     |   print(" ")
     | }
one two three 
//使用scala循规蹈矩，但是可读性极强的定义方式
scala> val greetings_2:Array[String] = new Array[String](3)
greetings_2: Array[String] = Array(null, null, null)
scala> greetings_2(0)="one"
scala> greetings_2(1)="two"
scala> greetings_2(2)="three"
scala> for (i <- 0 to 2)
     | {
     |   print(greetings_2(i))
     |   print(" ")
     | }
one two three 
//更加简洁的创建方式
scala> val numbers  = Array("one","two","thre")
numbers: Array[String] = Array(one, two, thre)
```

本例中实例参数(array.size=3)化了一个对象数组。scala和java的一个较大的差别，scala的数组使用的是()来定位数组元素而不是[]，后者是java中的方法，原因是：。

- scala中的操作符

scala中没有传统意义上的操作符，+-*/都是`方法字符 ` 

```scala
scala> (1).+(2)
res10: Int = 3

scala> (1).+2
<console>:1: error: ';' expected but integer literal found.
       (1).+2
            ^

scala> 1.+(2) #<-----方法体的示范，解释二四error的原因
res11: Int = 3

scala> 1.+2
<console>:1: error: ';' expected but integer literal found.
       1.+2
          ^
```

> 下面关于括号访问的解释，可能不会一下子明白
>
> 用括号传递给变量一个或者多个值参数时，scala会把他们成对转换成对**apply**方法的调用。
>
> greetings(i)$\leftrightarrow$greetings.apply(i)
>
> 所以scala里访问数组元素和其他`方法调用`一样，即也是一种方法调用。
>
> 前提是这个类型定义过apply方法！
>
> scala中还有update方法，使用该方法对参数变量进行赋值操作
>
> 下面是一个完整的表述案例
>
> -----------------------------------------------------------------
>
> *val greetings:Array[String] = new Array[String]\(3)*
>
> *greetings.update(0,"Hello")*
>
> *greetings.update(1,",")*
>
> *greetings.update(2,"JJ")*
>
> *for(i<-0 to 2)*
>
> *{*
>
> ​	println(greetings.apply(i))
>
> *}*
>
> ----------------------------------------------------------------------------------------------
>
> 但是对于更加深层次的原理现在还不清楚，也许后面会展开叙述

- 使用列表List

1. 问题：什么是函数式编程中的推迟方法副作用，以及方法没有副作用？

回答：计算然后返回值，是这个方法存在的唯一目的！降低方法间的耦合度，更加可靠易于复用。这里涉及到两种语言的思想：函数不可变性和对象不可变性。

**识别副作用的一个方法：看函数是否返回Unit**

2. Array[类型]\(参数)化数组，一旦创建完毕，本身大小不可变，数组内容可变。所以Array还是可以变化的！List才是真正的不变同类对象序列

关于List目前可以掌握创建以及使用如下代码演示：

```scala
scala> val l1 = List(1)
l1: List[Int] = List(1)

scala> val l12 = List(1,2)
l12: List[Int] = List(1, 2)

scala> val l34 = List(3,4)
l34: List[Int] = List(3, 4)
//两个List的合并方法
scala> val l1234 = l12:::l34
l1234: List[Int] = List(1, 2, 3, 4)
//List和同类数组合并的方法
scala> val l134 = 1::l34
l134: List[Int] = List(1, 3, 4)
//错误示范
scala> val l134 = l1::l34
l134: List[Any] = List(List(1), 3, 4)
//使用前缀符号创建List 那么思考题：下面的构造顺序是怎样的？
scala> val l5678 = 5::6::7::8::Nil //如何巧用Nil空list来构造一个调用对象
l5678: List[Int] = List(5, 6, 7, 8)
//::方法本质的探究，以及为什么称之为前缀符号
scala> val ltype =l1.::(2)
ltype: List[Int] = List(2, 1)
//新问题，：：后的存储结构是什么样的？

//一个list的创建以及其所支持的方法
scala> val  functest = List("Will","fill","until","JJ")
functest: List[String] = List(Will, fill, until, JJ)         

scala> functest.
++              flatMap              min                 sortBy          
++:             flatten              minBy               sortWith        
+:              fold                 mkString            sorted          
/:              foldLeft             nonEmpty            span            
:+              foldRight            orElse              splitAt         
::              forall               padTo               startsWith      
:::             foreach              par                 stringPrefix    
:\              genericBuilder       partition           sum             
WithFilter      groupBy              patch               tail            
addString       grouped              permutations        tails           
aggregate       hasDefiniteSize      prefixLength        take            
andThen         hashCode             product             takeRight       
apply           head                 productArity        takeWhile       
applyOrElse     headOption           productElement      to              
canEqual        indexOf              productIterator     toArray         
collect         indexOfSlice         productPrefix       toBuffer        
collectFirst    indexWhere           reduce              toIndexedSeq    
combinations    indices              reduceLeft          toIterable      
companion       init                 reduceLeftOption    toIterator      
compose         inits                reduceOption        toList          
contains        intersect            reduceRight         toMap           
containsSlice   isDefinedAt          reduceRightOption   toSeq           
copyToArray     isEmpty              repr                toSet           
copyToBuffer    isTraversableAgain   reverse             toStream        
corresponds     iterator             reverseIterator     toString        
count           last                 reverseMap          toTraversable   
diff            lastIndexOf          reverse_:::         toVector        
distinct        lastIndexOfSlice     runWith             transpose       
drop            lastIndexWhere       sameElements        union           
dropRight       lastOption           scan                unzip           
dropWhile       length               scanLeft            unzip3          
endsWith        lengthCompare        scanRight           updated         
equals          lift                 segmentLength       view            
exists          map                  seq                 withFilter      
filter          mapConserve          size                zip             
filterNot       max                  slice               zipAll          
find            maxBy                sliding             zipWithIndex  
// 常见几种的测试
scala> functest(2)
res15: String = until
// 条件计数
scala> functest.count(p=>p.length==4)
res17: Int = 2
// 丢弃再返回一个新的数组
scala> functest.drop(2)
res18: List[String] = List(until, JJ)
// 验证上面一条，drop并不会改变原始数组
scala> functest
res19: List[String] = List(Will, fill, until, JJ)
// 存在
scala> functest.exists(s=>s=="until")
res20: Boolean = true
// 对于任意
scala> functest.forall(s=>s.endsWith("l"))
res24: Boolean = false
// 过滤器
scala> functest.filter(s=>s=="until")
res21: List[String] = List(until)
// foreach 迭代器
scala> functest.foreach(print)
WillfilluntilJJ
scala> functest.foreach(println)
Will
fill
until
JJ
```

- 使用元组tuple

1. 问题：为什么元组被称之为容器对象，以及什么是容器对象？
2. 元组也是不可变对象，到目前为止Array是一定意义上的可变对象，List是不可变对象。但是元组中包含的类型是非唯一的

了解元组的创建以及访问，关于元组访问不能直接()的原因：apply返回值相同。但是需要注意的是_N的访问方式是从一开始的

```scala
scala> val tuple = ("99",1)
tuple: (String, Int) = (99,1)

scala> tuple._1 //十分注意此时为什么是1而不是0，为什么是99而不是1
res28: String = 99
```

- 集合(set)以及映射(map)

1. scala中的set，scala的API包含了set的`基本特质` (trait)，两个子特质：`可变set`和`不可变set`。

```scala
scala> var jetSet = Set("o","k")
jetSet: scala.collection.immutable.Set[String] = Set(o, k)

scala> jetSet
res33: scala.collection.immutable.Set[String] = Set(o, k)

scala> jetSet+="!" //这一操作，不可变Set：immutable执行的是产生返新集合

scala> jetSet
res35: scala.collection.immutable.Set[String] = Set(o, k, !)
// 导入可变的Set
scala> import scala.collection.mutable.Set
import scala.collection.mutable.Set

scala> var mvSet = Set("o","k")
mvSet: scala.collection.mutable.Set[String] = Set(k, o)

scala> mvSet+="?"//这一操作，可变Set：mutable执行的是对自身的操作
res36: scala.collection.mutable.Set[String] = Set(?, k, o)
//并且插入顺序是?顺序
scala> mvSet+="?"
res37: scala.collection.mutable.Set[String] = Set(?, k, o)
scala> mvSet+="?？"
res38: scala.collection.mutable.Set[String] = Set(?, k, o, ?？)
scala> mvSet+="a"
res39: scala.collection.mutable.Set[String] = Set(?, k, o, a, ?？)

```

2. map同样有可变以及不可变map

```scala
scala> import scala.collection.mutable.Map
import scala.collection.mutable.Map

scala> val mapppp = Map[Int,String]()
mapppp: scala.collection.mutable.Map[Int,String] = Map()

scala> mapppp+=(1->"one")
res49: mapppp.type = Map(1 -> one)

scala> mapppp+=(2->"two")
res50: mapppp.type = Map(2 -> two, 1 -> one)

scala> mapppp(1)
res51: String = one

scala> mapppp(2)
res52: String = two

//更加快捷的定义方式
scala> val mapfast = Map(1->"I",2->"II",3->"III")
mapfast: scala.collection.mutable.Map[Int,String] = Map(2 -> II, 1 -> I, 3 -> III)
```

对于set和map是第一次接触到import以及scala的特质(trait)，需要留意

- 学习函数式编程 - 尝试不使用var，仅仅使用val

指令式

```scala
def printArgs(args:Array[String]):Unit={
	var i = 0
  while ( i<args.length){
  	println(args(i))
    i+=1
  }
}
```

进度1

```scala
def printArgs(args:Array[String]):Unit={
	for(arg<-args)println(arg)
}
```

但是这样的对比暂时不能引起我的思考,R,java foreach都是类似的编程方法

进度2

```scala
def printArgs(args:Array[String]):Unit={
	args.foreach(println)
}
```

这样才有点意思，但是R语言，以及python里都有类似的apply。

**问题：怎样才是一个没有副作用的函数！？**

- 从文件里读取文本行

脚本`fileio.scala`

```scala
import scala.io.Source
if(args.length>0){
  for(line<-Source.fromFile(args(0)).getLines)
    println(line.length+":"+line)
}else
  Console.err.println("Please Input file name!")
```

执行脚本

```bash
:~/Scala/SLN/sln$ scala fileio.scala README.md
5:# SLN
0:
23:Scala Learning Notebook
0:
16:这是一本Scala的学习笔记  
16:参考文件Scala编程完整版  
16:创建时间 11/2-2018  
```

总结：

1. 学习了几种集合实现，注意区分可变和不可变

- Array[Type] 可变

- List 不可变

- Tuple  由于允许多元，所与访问方式特殊

- Set 可变set特质&不可变set特质 scala.collection.immutable.Set 不可变Set

- Map 可变map特质&不可变map特质 scala.collection.mutable.Map 可变Map

  可变与不可变在实现+时的差距或差别？

2. 初识apply、update函数，访问即调用
3. 初识特质（trait），类似java接口？
4. 什么被称之为副作用，怎么样又算是没有副作用?看返回值是否是Unit
5. 在命令行中如何发现方法，发现方法定义？`Tab`或者`试错`
6. 文件io所使用的类`scala.io.Source` 
7. 函数式编程思想：形成过程中



