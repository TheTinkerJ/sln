# scala_day5

## scala中的内建控制结构

scala中的内建控制结构比较少，原因是因为Scala从语法层面支持函数字面量。所有的scala控制结构都会产生某个值，就像方法一样，包括赋值语句 ( line = readLine() ) 其返回结果就是Unit类型。Scala的内建控制结构仅有几种：

- if 以及其返回值

- while 以及其返回值类型是Unit值

  纯函数式语言中不包含while因为这个体没有返回值，scala包含之，是为了方便程序员？能用递归的就不会使用while？

  对比下面两段

  - while loop 形式

  ```scala
  def gcdLoop(x:Long,y:Long):Long = {
    var a = x
    var b = y
    while(a != 0 ){
    	var tmp = a
      a = b%a
      b = tmp;
    }
  	b 
  }
  ```

  - 递归形式

  ```scala
  def gcd(x:Long,y:Long):Long = 
  	if (y == 0) x else gcd( y ,x % y)
  ```

- for

  for表达式是虽然之前提过，在scala中他只有一种应用方法，但是在接下来的观察者发现for表示的用法真可谓是脑洞大开！

  关于for表达式需要说的有以下几点：

  - 发生器

  - 过滤器以及滤间绑定 r如果scala中加入超过一个过滤器则需要加；但是如果使用的是花括号则不需要

  - 嵌套枚举 

  - 创造新集合

    --------------------------------------------------------------------

     `for {子句} yield {循环体}`       ！关于循环体的实现仍然==存在疑问==！

    -------------------------------------------------------------------------------

  ```scala
  :~/Scala/SLN/sln$ scala
  Welcome to Scala 2.12.7 (OpenJDK 64-Bit Server VM, Java 10.0.2).
  Type in expressions for evaluation. Or try :help.
  
  scala>  val filesHere = (new java.io.File(".")).listFiles
  
  scala> for (file <- filesHere if file.getName.endsWith("scala"))
       | println(file)
  ./pa.scala
  ./Summer.scala
  ./hello.scala
  ./fileio.scala
  ./helloarg.scala
  ./ChecksumAccumulator.scala
  ./FallWinterSpringSummer.scala
  ./forargs.scala
  ./printarg.scala
  ./Rational.scala
  
  scala> for (file <- filesHere)
       | println(file)
  ./pa.scala
  ./Summer.scala
  ./scala_day1.md
  ./scala_day4.md
  ./hello.scala
  ./fileio.scala
  ./scala_day2.md
  ./helloarg.scala
  ./ChecksumAccumulator.scala
  ./scala_day3.md
  ./scala_day5.md
  ./FallWinterSpringSummer.scala
  ./README.md
  ./forargs.scala
  ./.git
  ./printarg.scala
  ./Rational.scala
  
  scala> for (file <- filesHere if !file.getName.endsWith("scala"))
       | println(file)
  ./scala_day1.md
  ./scala_day4.md
  ./scala_day2.md
  ./scala_day3.md
  ./scala_day5.md
  ./README.md
  ./.git
  
  scala> for (file <- filesHere  if ! file.getName.endsWith("scala")) yield 
       | file.getName
  res9: Array[String] = Array(scala_day1.md, scala_day4.md, scala_day2.md, scala_day3.md, scala_day5.md, README.md, .git)
  
  scala> for {
       |   file <- filesHere
       |   filename = file.getName.toString
       |   if filename.endsWith(".scala")
       | }yield filename
  res12: Array[String] = Array(pa.scala, Summer.scala, hello.scala, fileio.scala, helloarg.scala, ChecksumAccumulator.scala, FallWinterSpringSummer.scala, forargs.scala, printarg.scala, Rational.scala)
  
  scala> for {
       |   file <- filesHere
       |   filename = file.getName
       |   if filename.endsWith(".scala")
       | }yield filename
  res19: Array[String] = Array(pa.scala, Summer.scala, hello.scala, fileio.scala, helloarg.scala, ChecksumAccumulator.scala, FallWinterSpringSummer.scala, forargs.scala, printarg.scala, Rational.scala)
  
  scala> for {
       |   file <- filesHere
       |   filename = file.getName
       |   if filename.endsWith(".scala")
       | }yield filename {
       |   println(filename)
       | }
  <console>:20: error: type mismatch;
   found   : Unit
   required: Int
           println(filename)
  
  //与下面的例子形成对比
  scala> for (
       |   file <- filesHere
       |   filename = file.getName
       |   if filename.endsWith(".scala")
       | ) yield filename {
       | println(filename)
       | }
  <console>:17: error: value filename is not a member of Array[java.io.File]
  possible cause: maybe a semicolon is missing before `value filename'?
           filename = file.getName
           ^
  //分号与花括号的问题 解决
  scala> for (
       |   file <- filesHere;
       |   filename = file.getName;
       |   if filename.endsWith(".scala")
       | ) yield filename 
  res24: Array[String] = Array(pa.scala, Summer.scala, hello.scala, fileio.scala, helloarg.scala, ChecksumAccumulator.scala, FallWinterSpringSummer.scala, forargs.scala, printarg.scala, Rational.scala)
  //形成对比！！！
  scala> for {
       |   file <- filesHere
       |   filename = file.getName
       |   if filename.endsWith(".scala")
       | }yield filename
  res19: Array[String] = Array(pa.scala, Summer.scala, hello.scala, fileio.scala, helloarg.scala, ChecksumAccumulator.scala, FallWinterSpringSummer.scala, forargs.scala, printarg.scala, Rational.scala)
  
  ```

- try

  - 抛出异常

  - 捕获异常

  - finally子句

    finally子句中的返回语句具有无比高的优先级，甚至比没有异常的try体中的return还要高。catch中的异常捕获以后依次匹配，如果无法匹配成功最后try - catch 将把异常上升出去

    ```scala
    scala> def f():Int = try{return 1}finally{return 2}
    f: ()Int
    scala> def f2():Int = try{ 1 }finally{ 2 }
    <console>:11: warning: a pure expression does nothing in statement position
           def f2():Int = try{ 1 }finally{ 2 }
                                           ^
    f2: ()Int
    
    scala> f
    res27: Int = 2
    
    scala> f2
    res28: Int = 1
    
    ```

    try{}catch{}finally{}也有生成值。返回结果：

    1. 没有异常 try
    2. 异常捕获 catch
    3. 未捕获 表达式没有返回值
    4. finally有时候也会抛出异常值

- match

  类似switch-case区别有两点：

  1. 任意类型
  2. 没有break
  3. 默认default在scala中是_



完毕！



