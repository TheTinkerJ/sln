# Scala day 4

## 基本类型和操作

- `Btye` 		
- `Short`
- `Int`
- `Long`
- `Char`
- `String`
- `Float`
- `Double`
- `Boolean`

### 字面量

解释：在计算机中所有内容都有存储形式以及表示形式，用化学术语来解释就是亲人和亲机器。存储

形式是亲机器，字面量是亲人，即更加适合表示给人看。

### 操作符和方法

- 前缀操作符

  **一元操作符** 操作符位于对象之后

  ```scala
  - 7
  ```

- 中缀操作符

  调用的方法位于 对象和参数之间

  ```scala
  s indexof 'o'
  1 + 2
  ```

- 后缀操作符

  **一元操作符** 操作符位于对象之后

  ```scala
  7 toLong
  ```

- 方法

  X缀操作符号和方法的区别在于不需要像`Java`一样`.()`

### 对象的相等性

- scala与Java中 == 的区别

1. java中 ==

   1. 原始类型 ：值比较
   2. 引用类型 ：引用相等性比较

   这样的机制在scala中叫做 `eq`

2. 所以在scala中没有这两种比较方式，只支持值比较？



### Part 1 .总结

这里的内容比较偏概念



# 函数式对象

`Rational`有理数类的构建

```scala
class Rational (n:Int , d:Int){

  //参数列表合法检验
  require(d != 0)  //分母不为0

  //参数构造阶段
  private val g = gcd( n.abs , d.abs ) //求解最大公约数 在初始化阶段即完成最简
  val numer = n/g     //解决自身方法无法访问参数列表问题
  val denom = d/g
  def this(n:Int) = this(n,1)       //解决多参数构造器问题

  //方法定义 + - * / 使用重载来满足 Int 和 Rational的多种参数输入

  def +(that:Rational) : Rational =
    new Rational(this.numer*that.denom + that.numer*this.denom,this.denom*that.denom)

  def +(i:Int):Rational =
    new Rational(this.numer+i*this.denom,this.denom)

  def -(that:Rational) : Rational =
    new Rational(this.numer*that.denom - that.numer*this.denom,this.denom*that.denom)

  def -(i:Int):Rational =
    new Rational(this.numer-i*this.denom,this.denom)

  def *(that:Rational) : Rational =
    new Rational(this.numer*that.numer,this.denom*that.denom)

  def *(i:Int):Rational =
    new Rational(this.numer*i,this.denom)

  def /(that:Rational) : Rational =
    new Rational(this.numer*that.denom ,this.denom*this.numer)

  def /(i:Int):Rational =
    new Rational(this.numer,this.denom*i)

  override def toString = numer +"/"+ denom  //覆盖超类java.lang.object的方法
  private def gcd(a:Int,b:Int):Int =
    if( b == 0 ) a else gcd ( b , a % b )//解决分子分母化简
}

```



上述代码中的几点疑惑以及潜在知识点

- 类的定义

  ```scala
  class Name(ValueList:type) {
  
  	require(boolean-expression)
  	
    [private] val consistant-value = ValueList or Other
    
    construction expression
   
    def this(...)... //constructor
    
    def function1(ValueList:type):returnType = {}
  	
    [private] def function2(ValueList:type) = {}
  
  }
  ```

一个类体中需要构造，构造方法，类参数，输入参数列表，方法定义等等

- require `先决条件`

  确保对象创建时数据的有效性，该方法接受一个布尔表达式用来确认构造有效性

- 添加类字段以提供调用访问

- this关键字的两种用法

  - 子指向 `this.[参数|方法]`

  - 辅助构造器 主构造器之外的其他构造器被称为辅助构造器

    this(...) = {

    ​	this(...) //第一个动作调用同类其他构造器(`主构造器`或者定义在`前面`的构造器)

    }

  主构造器：Scala编译器会将类内部既不是字段也不是方法的定义的代码编译至主构造器中，所以主构造器是一个隐式的基于类定义的构造器。因此主构造器是类的唯一入口。

- 访问控制：私有字段以及方法 
- 方法的重载
- 方法的命名 `驼峰法`
- 类的定义 单词首字母大写
- scala中标识符的规则 $ 与 _ 减少使用



上述代码在cmd中的使用：

```bash

:~/Scala/SLN/sln$ scalac Rational.scala
:~/Scala/SLN/sln$ scala
Welcome to Scala 2.12.7 (OpenJDK 64-Bit Server VM, Java 10.0.2).
Type in expressions for evaluation. Or try :help.

scala> import Rational._  //导入Rational.class中的全部类
import Rational._

scala> val f1z2 = new Rational(1,2)
f1z2: Rational = 1/2

scala> val f1z3 = new Rational(1,3)
f1z3: Rational = 1/3

scala> f1z2+f1z3
res0: Rational = 5/6

scala> f1z2-f1z3
res1: Rational = 1/6

scala> f1z3-f1z2
res2: Rational = -1/6

scala> f1z3-1
res3: Rational = -2/3

```

























