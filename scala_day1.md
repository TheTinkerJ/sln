# Scala导论

## 学习路线设计：

1. 安装Scala
   1. SBT
   2. Jupyter
   3. VsCode
2. 学习基本语法
3. 学习语言特色
4. 学习高级特性
5. 与Spark对接



## 学习环境

ubuntu18.04 lts



## 安装Scala

- 在官方网站下载 [scala-2.12.7.deb](https://downloads.lightbend.com/scala/2.12.7/scala-2.12.7.deb)

- 安装 Java 

  `sudo apt install openjdk-11-jdk-headless`

- 直接安装下载的 `scala-2.12.7.deb` 

- 检查安装情况

  ```bash
  thetinkerj@JJ:~$ scala
  Welcome to Scala 2.12.7 (OpenJDK 64-Bit Server VM, Java 10.0.2).
  Type in expressions for evaluation. Or try :help.
  
  scala> 
  ```

命令行式的Scala环境开发成功！



## Scala学习笔记

### 解释器的使用

- ```scala
  scala> 1+2
  res0: Int = 3 
  ```

  结果0；类型 Int； 结果= 3

  Int类型对应Java的int，广泛意义上来说，Java中的所有原始类型在scala中都有对应的类型

  Scala最终会将代码转换为Java字节码，从而加速

- ```scala
  scala> 1+2
  res0: Int = 3
  
  scala> res0*3
  res1: Int = 9
  
  scala> println("hello,world!")
  hello,world!
  ```

  可以看到Scala对结果的处理[res0,res1...]，Scala的println与Java的System.out.println一样

### 变量的定义

- ```scala
  scala> val msg="hello world"
  msg: String = hello world
  
  scala> println(msg)
  hello world
  ```

  Scala的两种变量种类

  - val-Java中的final变量，只能被赋值一次
  - var-Java中的非final变量，可以在生命周期中赋值多次

  scala具备`类型推断`能力，所以Scala既可以使用他的类型推断能力， 也可以指明类型,，这样可以增加可读性

- ```scala
  scala> val msg:java.lang.String="hello,again,world!"
  msg: String = hello,again,world!
  
  scala> val msg2:String="hello,again,world!"
  msg2: String = hello,again,world!
  ```

  所以是否可以如下解读

  变量种类[val] - 变量名以及类型[msg:String] - 等于[=] - 值["Hello World！"]

  其中变量种类只有两种，即`val`和`var`

- ```scala
  scala> msg = "111"
  <console>:12: error: reassignment to val
         msg = "111"
  
  scala> var msg1 = "hellow "
  msg1: String = "hellow "
  
  scala> msg1 = "hello world!"
  msg1: String = hello world!
  ```

  val变量只能够被赋值一次，如果再次尝试赋值就会出现错误：对val类型变量重复赋值；而var变量是可以重新赋值的

- ```scala
  scala> var msg_multiline = 
       | "This is a line \n This is another line"
  msg_multiline: String =
  This is a line
   This is another line
  ```

  多行输入，采用转义字符序列

### 函数定义

- ```scala
  scala> def max(x:Int,y:Int):Int = (
       |   if(x>y)x
       |   else y
       | )
  max: (x: Int, y: Int)Int
  
  scala> def max(x:Int,y:Int):Int = {
       |   if (x>y)
       |     x
       |   else
       |     y
       | }
  max: (x: Int, y: Int)Int
  
  scala> def max(x:Int,y:Int):Int=if(x>y) x else y
  max: (x: Int, y: Int)Int
  
  scala> max(3,5)
  res2: Int = 5
  ```

  def 函数名max(括号中是参数列表：类型)：返回值类型 = {函数定义}；这是一种带参数，带返回值的全面的函数定义。

### Scala脚本的编写

scala脚本`hello.scala`最基本

```bash
$vim hello.scala
```

scala脚本内容`hello.scala`

```scala
println("hello,world!from a script!")
```

执行该脚本

```bash
$ scala hello.scala
hello,world!from a script!
```

scala脚本`helloarg.scala`可以输入参数

```scala
println("hello "+args(0)+" !")
```

执行该脚本

```bash
$ scala helloarg.scala JJ
hello JJ !
```

### while循环-if判断

scala脚本`printargs.scala`，这里的编程风格被称之为指令式风格

```bash
var i=0
while(i < args.length){
  println(args(i))
  i+=1
}
```

执行脚本

```bash
$ scala printarg.scala JJ FM AM HELLO XL CXL JJ LOVE
JJ
FM
AM
HELLO
XL
CXL
JJ
LOVE
```

### foreach和for枚举

scala的编程倾向于`函数式编程`，函数式编程的主要特征是：函数是头等结构。

函数的字面语法构成包含如下部分:括号及命名参数列表，右箭头以及函数体

```scala
(x: Int,y: Int) => x + y
```

scala脚本`pa.scala`

```scala
args.foreach(arg => println(arg))
//也可以指明参数类型
args.foreach((arg:String) => println(arg))
//甚至可以直接将函数传递进来
args.foreach(println)
```

执行脚本

```bash
$ scala pa.scala one two three
one
two
three
```

scala只有一个指令式for，即只有一种for表达式”for expression“

scala脚本`forargs.scala`

```scala
for(arg<-args)
	println(arg)
```

执行脚本

```bash
$ scala forargs.scala one two three
one
two
three
```

## 总结

学习了scala的下列内容：

- 变量两种val和var
- 了解了一个完整的scala的函数体定义
- 如何编写和执行scala的脚本
- while循环体，if判断体
- foreach以及for的唯一表达式

比较重要的一个概念是函数式编程，以及函数作为scala的头等结构是如何实现的？这也是接下来学习要十分留意的一个方向！