# scala day_3

## 前言

在前面两次的学习中，初识scala，学习了以下内容：

1. scala中变量的两种定义

   val与var，val只能被赋值一次，一旦创建不能改变，var可以在使用过程中被多次赋值，同时scala具备类型推断能力。所以下示定义方式均为合格

   ```scala
   var msg = "hello"
   var msg:String = "hello"
   var msg:java.lang.String = "hello"
   ```

2. scala中函数的定义？什么样的函数是好函数？怎么才能消除函数的辅作用？如何培养函数式编程思维？

   函数的定义

   ```scala
   def function(variable:Type,...):output={definition}
   ```

3. scala脚本的编写(xxxx.scala)以及执行(scala xxxx.scala0)

4. while循环 if判断

   ```scala
   while(...){...}
   if()...else...
   for(arg<-args){...} //唯一用法 
   args.foreach(...)
   ```

5. foreach for枚举，for在scala中只有一种用法

6. 数组集合：Array[Type]、List、Tuple、Set、Map

   其中注意区分可变与不可变，以及各自的定义模式

   List的::方法，NIL量

   访问时的apply方法(非tuple)与非apply方法(tuple)

7. 初识特质trait
8. 文件io：scala.io.Source

9. 函数式编程的概念



## 类和对象

### 类，字段、方法

期望：分号推断？单例对象？

#### 1. 定义与实例化

```scala
scala> class DAY{
     |   private var day:Int = 1//带访问控制权限的变量定义方式
     |   def add() {day+=1}     //无等号的函数定义 默认返回Unit 
     |   def today():Int={day}  //带等号的函数定义 返回定义类型  	
     | }
defined class DAY

scala> var d = new DAY //实例化
d: DAY = DAY@3c64339f

scala> d.add() 
scala> d.today()
res2: Int = 2
scala> d.add
scala> d.today //可以看出在写代码是scala的自由度相当高
res4: Int = 3
scala> d.add
scala> d.today //这是这样会给阅读带来麻烦吗？
res6: Int = 4
```

- scala中的默认访问控制权限是public，那么方法可以添加访问控制权限吗，这么问是不是有点指令式or面向对象式思维？
- Unit类型到底是什么类型？

#### 2. 分号推断

**分号推断规则：**（三条均不成立，则默认具有括号）

1. **疑问行由一个不能合法作为语句结尾的字结束，如句点或中缀操作符**
2. **下一行开始不能作为语句开始的词**
3. **行结束于括号(...)或[...]内部，因为这些符号不可能容纳多个语句**

#### 3.Singleton对象

scala不能定义静态成员，取而代之的是单例对象。单例对象和普通对象的唯一区别 $object \leftrightarrow class$

看一段代码

```scala
import scala.collection.mutable.Map


class ChecksumAccumulator{
  private var sum = 0
  def add(b:Byte){ sum += b }
	def checksum():Int = ~(sum & 0xFF) + 1
}

object ChecksumAccumulator {
	private val cache = Map[String,Int]()
  def calculate(s:String):Int = 
  	if (cache.contains(s))										//缓存中有就直接返回，没有就执行计算
  		cache(s)
  	else{
      val acc = new ChecksumAccumlator				//创建一个码校验器生成器
      for( c <- s)
      	acc.add(c.toByte)											//添加值
      val cs = acc.checksum()									//计算校验码
      cache += (s -> cs) 											//才map中添加一组映射
      cs																			//返回该cs的校验码
		}
}
```

本例中存在一个类和一个单例对象，此时单例对象与类同名也被称之为“**伴生对象**”

类和它的伴生对象必须定义在**一个源文件**里，而此时类称为单例对象的"**伴生类**"

两者可以相互访问私有成员：`打破访问控制的壁垒`

单例对象不只是`静态方法的工具类`它同样是头等的对象，所以单例对象的名字可以被看作是贴在对象上的标签！所以说，scala没有静态成员！而是通过单例对象去做类似的访问

- **解释Java中静态方法工具类？**好像有点映像：Integer.MAXIMUN

- **类和单例对象的差别?**单例对象不带参数，而类可以。因为单例对象不需要使用new关键字实例化，每一个单例对象都被实现为`虚构类`的实例，并指向`静态变量`,**单例对象在第一次被访问的时候才会被初始化**。

不与伴生类共享名称的单例对象被称之为“**独立对象**”

### 4.scala程序

- main方法程序的入口，唯一参数Array[String]，返回结果Unit

- 任何拥有合适`签名`的**单例对象**都可以用来作为程序的入口

- scala每一个源文件都默认包含了对`java.lang`,`scala`,`单例对象Predef`的成员引用。

  println 实际是Predef.println。

- scala对程序文件(脚本)的命名没有硬性规定但是可以参造JAVA因为比较好找Bug

#### scala编译器

- scalac filelist... //但是会比较慢

- fsc 快速scala编译器 fast scala compiler

  `fsc filelist...`      //启动

  `fsc -shutdown`		//关闭

#### scala运行编译生成

​	`scala generated_exec_name`

### 5.Applicaion特质

看代码

```scala
import ChecksumAccumulator.calculate

object FallWinterSpringSummer extends Application {					//在单例对象后混入Application
	for (season <- List("Fall","Winter","Spring","Summer")) 
  	println(season+":"+calculate(season))
}
```

在单例对象后混入Application特质，然后就可以代之main方法！ 单例对象{}里的代码就会自动顺序执行。这样比写mian方法要简单。

### 总结

#### 将所有代码串编一遍！

scala文件一：`ChecksumAccumulator.scala`

```scala
import scala.collection.mutable.Map 						//导入可变Map

class ChecksumAccumulator{											//定义类

  private var sum = 0			
  def add(b:Byte){ sum += b }								
  def checksum():Int = ~(sum & 0xFF) + 1

}

object ChecksumAccumulator {										//定义单例对象

  private val cache = Map[String,Int]()
  def calculate(s:String):Int =
    if (cache.contains(s))
      cache(s)
    else{
      val acc = new ChecksumAccumulator
      for( c <- s)
        acc.add(c.toByte)
      val cs = acc.checksum()
      cache += (s -> cs)
      cs
    }
}
```

scala文件二：`Summer.scala`

```scala
import ChecksumAccumulator.calculate

object Summer{

  def main(args:Array[String]){									//程序入口，单例对象混入Application特质
    for (arg<-args)
      println(arg+" : "+calculate(arg))
  }

}
```

编译：采用`scalac`

```bash
:~/Scala/SLN/sln$ scalac ChecksumAccumulator.scala Summer.scala
:~/Scala/SLN/sln$ ls
ChecksumAccumulator.class 'ChecksumAccumulator$.class' ChecksumAccumulator.scala Summer.class 'Summer$.class' Summer.scala 
```

执行生成的java字节码文件

```bash
:~/Scala/SLN/sln$ scala Summer JJ go!
JJ : -148
go! : -247
```

使单例对象混入Application特质(trait:**注在scala2.9以后的版本已经变成了App**)

#### scala文件`FallWinterSpringSummer.scala`

```scala
import ChecksumAccumulator.calculate

object FallWinterSpringSummer extends App{

  for(season<-List("fall","winter","spring","summer"))
    println(season+":"+calculate(season))

}
```

编译执行

```bash
:~/Scala/SLN/sln$ scalac ChecksumAccumulator.scala  FallWinterSpringSummer.scala
:~/Scala/SLN/sln$ scala FallWinterSpringSummer
fall:-159
winter:-153
spring:-147
summer:-153
```

可以看出App{}内的代码被执行了！