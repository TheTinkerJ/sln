# scala_day6

## 函数和闭包

把代码分割成函数，将冗余的程序分割成更小更易于管理的方式。

- 对象的成员函数 - 方法
- 函数中的函数
- 函数字面量
- 函数值



### 方法

示例代码

`FindLongLines.scala`

```scala
import scala.io.Source

object LongLines {	
    def processFiles(filename:String,width:Int){
        val source = Source.fromFile(filename)
        for(line <- source.getLines)
            processLine(filename,width,line)
    }

    private def processLine(filename:String,width:Int,line:String){
        if(line.length > width)
            println(filename+" : "+line.trim)
    }
}

object FindLongLines {

    def main(args:Array[String]){	 //单例对象接入main方法成为程序入口
        val width = args(0).toInt	 //类型转换
        for( arg <- args.drop(1)){   //丢弃调一个元素
            LongLines.processFiles(arg,width) //单例对象有静态对象的使用方法
        }
    }
}
```

使用该代码：

```bash
:~/Scala/SLN/sln$ scala FindLongLines.scala  45 FindLongLines.scala 
FindLongLines.scala : def processFiles(filename:String,width:Int){
FindLongLines.scala : val source = Source.fromFile(filename)
FindLongLines.scala : private def processLine(filename:String,width:Int,line:String){
```

### 函数的函数：本地代码

FindLodalFunc.scala

```scala
import scala.io.Source

object LongLines {
    def processFiles(filename:String,width:Int){

        //将方法定义到函数中：本地方法-函数的函数
        def processLine(filename:String,width:Int,line:String){
            if(line.length > width)
                println(filename+" : "+line.trim)
        }

        val source = Source.fromFile(filename)
        for(line <- source.getLines)
            processLine(filename,width,line)
    }
}

object FindLongLines {

    def main(args:Array[String]){
        val width = args(0).toInt
        for( arg <- args.drop(1)){
            LongLines.processFiles(arg,width)
        }
    }
}
```

执行代码

```bash
:~/Scala/SLN/sln$ scala FindLocalFunc.scala  45 FindLocalFunc.scala 
FindLocalFunc.scala : def processFiles(filename:String,width:Int){
FindLocalFunc.scala : def processLine(filename:String,width:Int,line:String){
FindLocalFunc.scala : println(filename+" : "+line.trim)
FindLocalFunc.scala : val source = Source.fromFile(filename)
```

### 头等函数 first-class function

将函数写成匿名的字面量，作为值传递

函数字面量和值的区别在于，函数字面量存在与源代码，函数值存在与运行期间 

这里的概念比较多 

- 函数的字面量  函数的名称

- 匿名的字面量 `(x:Int)=>println(x)` 整体就是一个没有命名的函数，输入参数是一个x,

  匿名函数就是一个`()=>()`这样的没有命名的函数

- 函数值 - 函数运行后的返回值

- 函数字面量的短形式（指匿名函数字面量）

  ```scala
  (x:Int) => x>0
  (x) => x>0
   x  => x>0
   x>0
   _ > 0 //这个像表情包一样的叫做 占位符号法
  ```

- 占位符号法 - 将使得函数字面量更加简洁

  - 代表一个参数

    ```scala
    val f = (_:Int)+(_:Int)
    ```

  - 代表参数列表

    ```scala
    println(_)
    println _
    ```

- 部分应用函数

  ```scala
  // 首先定义了一个函数，函数字面量是sum
  def sum(a:Int,b:Int,c:Int):Int = a + b + c
  ```

  部分应用函数是一种表达式，你不需要提供函数的所有参数。所以只是一种方便定义的方式？？

  ```scala
  // 定义了一个 变量，存了函数
  val a = sum _ //sum _ 就是一个sum的 部分应用表达式
  ```

  这里的下划线代表了全部参数，用于将一个本地函数，例如sum，包装成为一个与apply方法具有同样的参数列表和结果类型的`函数值`。

- 偏参数

  **函数未被应用于它所有的参赛**

  ```scala
  val b = sum(1,_:Int,3)
  ```

  因为提供了两个参数，仅缺少一个参数，Scala编译器会产生一个==新的函数类==，这个函数每次调用sum时会依次传入参数1，2，3。

### 闭包

函数字面量的传参问题

```scala
(x:Int) => x + more
```

more此时是一个自由变量 free varibale 

x此时是一个绑定变量 bound variable

```scala
val addMore = (x:Int) => x + more
```

解释：

- addMore 表示 匿名的函数字面量的值 即：函数值 也是==闭包==
- more是一个自由变量，需要被捕获，即需要预先被定义

因此有了以下用法：

```scala
def makeIncreaser(more:Int) = (x:Int) => x + more
val inc1   = makeIncreaser(1)
val inc100 = makeIncreaser(100) 
```

### 重复参数

```scala
def echo(args:String*) = {}
// 若传人 一个Array[String],则需要如下操作
echo(arr:_*) // :_*表示把arr铺开
```

### 尾递归

注意：

- 仅仅调用自身，才具有优化
- 使用尾递归来优化while，因为while没有循环



总结，本节比较全面浏览了scala的函数有了一个比较全面的认识！



















